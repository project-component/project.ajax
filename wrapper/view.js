
(function (window) {
    'use strict';

    if (window.jsProjectAjaxView) {
        return;
    }

    let jsProjectAjaxView = function () {
        let self = this;
        self.loader = new Array();
    };

    jsProjectAjaxView.prototype.add = function (func) {
        this.loader[this.loader ? this.loader.length : 0] = func;
    };

    jsProjectAjaxView.prototype.view = function() {
        for(let i in this.loader) {
            this.loader[i]();
        }
    };

    jsProjectAjaxView.prototype.removeParam = function (key, sourceURL) {
        var splitUrl = sourceURL.split('?'),
            rtn = splitUrl[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? splitUrl[1] : '';
        if (queryString !== '') {
            params_arr = queryString.split('&');
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split('=')[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + '?' + params_arr.join('&');
        }
        return rtn;
    };

    window.jsProjectAjaxView = new jsProjectAjaxView();
    $(function() {
        window.jsProjectAjaxView.view();
    });

})(window);