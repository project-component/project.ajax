<?php
$this->setFrameMode(true);
if (!\Project\Tools\Utility\User::isAutorize()
    || (!empty($arResult['IS_FORM_SEND']))) {
    global $arAuthResult;
    $APPLICATION->IncludeComponent(
        "bitrix:system.auth.forgotpasswd",
        'popup',
        [
            "AJAX_LOADER"    => $arResult,
            "AUTH_RESULT"    => $arAuthResult,
            "NOT_SHOW_LINKS" => 'Y',
        ]
    );
}