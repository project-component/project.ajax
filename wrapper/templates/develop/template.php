<?

$APPLICATION->IncludeComponent(
        "project.form:form",
        "develop",
        array(
            "WRAPPER" => $arResult,
            "EVENT_NAME" => "FORM",
            "IBLOCK_TYPE" => "zajavki",
            "IBLOCK_ID" => "5",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "NAME",
                1 => "EMAIL",
                2 => "MESSAGE",
                3 => "PHONE",
                4 => "",
            ),
            "REQUIRED_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "REQUIRED_PROPERTY_CODE" => array(
                0 => "NAME",
                1 => "PHONE",
                2 => "",
            ),
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "COMPONENT_TEMPLATE" => ".default"
        ),
        false
);
?>