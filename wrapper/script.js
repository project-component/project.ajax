(function (window) {
    'use strict';

    if (window.jsProjectAjaxWrapper) {
        return;
    }

    window.jsProjectAjaxWrapper = function (arParams) {
        let self = this;
        self.config = {
            ajax: arParams.AJAX,
            siteId: arParams.SITE_ID,
            contaner: arParams.CONTANER,
            contanerForm: arParams.CONTANER_FORM,
            contanerList: arParams.CONTANER_LIST,
            contanerMore: arParams.CONTANER_MORE,
            contanerPager: arParams.CONTANER_PAGER,
            contanerFilter: arParams.CONTANER_FILTER + ' .filter-element',
            contanerFilterAll: arParams.CONTANER_FILTER_ALL + ' .filter-element-all',
            start: false,
            stop: false
        };

        self.param = {
            action: 'AJAX',
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            FILTER: '',
            PAGEN_1: arParams.PAGEN ? parseInt(arParams.PAGEN) + 1 : 2,
            PAGE_URL: arParams.PAGE_URL,
            IS_UPDATE: 0,
            PARAM: arParams.PARAM
        };

        $(document).on('click', self.config.contanerMore, function () {
            self.loadAjax(this);
        });

        $(document).on('click', self.config.contanerFilter, function () {
            let param = $(this).data('filter');
            if (self.param.FILTER != param) {
                $(self.config.contanerFilter).each(function () {
                    if (param == $(this).data('filter')) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
                self.param.FILTER = param;
                self.update(this);
            }
        });

        if (self.config.contanerForm && $(self.config.contanerForm).is('form') && !$(self.config.contanerForm).data('custom-submit')) {
            $(document).on('submit', self.config.contanerForm, function () {
                self.submitHandler(this);
                self.sendPost(this, $(this).serialize());
                return false;
            });
        }
    };

    window.jsProjectAjaxWrapper.prototype.update = function (el) {
        this.param.PAGEN_1 = 1;
        this.param.IS_UPDATE = 1;
        this.loadAjax(el);
    };

    window.jsProjectAjaxWrapper.prototype.showNext = function (is, hidden) {
        if (!hidden) {
            hidden = 'hidden';
        }
        if (is) {
            $(this.config.contanerMore).parent().show();
        } else {
            $(this.config.contanerMore).parent().hide();
        }
        console.log('showNext', is);
    };

    window.jsProjectAjaxWrapper.prototype.loadAjax = function (el) {
        let self = this;
        let load = new window.projectAjaxLoader(el);

        $.get(self.config.ajax, self.param, function (data) {
            if (data) {
                self.param.PAGEN_1++;
                if (self.start) {
                    self.start(el);
                }
                if (self.param.IS_UPDATE) {
                    $(self.config.contaner).html($(self.config.contaner, data.content));
                } else {
                    $(self.config.contanerList).append($(self.config.contanerList, data.content).html());
                    $(self.config.contanerPager).html($(self.config.contanerPager, data.content).html());
                    eval($(data.content).find('script').text());
                }
                window.jsProjectAjaxView.view();

                load.success(self.stop);
                if (self.stop) {
                    self.stop(load.element());
                }
            } else {
                load.fail();
            }
            self.param.IS_UPDATE = 0;
        }, 'json').fail(function () {
            load.fail();
        });
    };

    window.jsProjectAjaxWrapper.prototype.submitHandler = function (form) {
    };

    window.jsProjectAjaxWrapper.prototype.sendPost = function (el, data) {
        let self = this;
        let load = new window.projectAjaxLoader(el);
        let param = self.param;
        let page = self.param.PAGEN_1;
        let update = self.param.IS_UPDATE;
        self.param.PAGEN_1 = 1;
        self.param.IS_UPDATE = 1;
        $.post(self.config.ajax + '?' + $.param(param), data, function (data) {
            self.param.IS_UPDATE = update;
            self.param.PAGEN_1 = 2;
            if (data) {
                if (self.start) {
                    self.start(el);
                }
                $(self.config.contaner).html($(self.config.contaner, data.content));
                // eval($(data.content).find('script').text());
                window.jsProjectAjaxView.view();
                load.success(self.stop);
                if (self.stop) {
                    self.stop(load.element());
                }
            } else {
                load.fail();
            }
        }, 'json').fail(function () {
            load.fail();
        });
    };

})(window);