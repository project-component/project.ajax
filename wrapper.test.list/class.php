<?php

class ProjectAjaxWrapperTestList extends CBitrixComponent {

    public function executeComponent() {
        $start = $this->arParams['WRAPPER']['PAGEN'] ?? 1;
        $limit = 2;
        $this->arResult['START'] = ($start - 1) * $limit + 1;
        $this->arResult['STOP'] = $start * $limit;
        for ($i = $this->arResult['START']; $i <= $this->arResult['STOP']; $i++) {
            $this->arResult['ITEMS'][] = array(
                'data-' . $i
            );
        }
        $this->arResult['NAV_RESULT'] = new stdClass();
        $this->arResult['NAV_RESULT']->NavPageNomer = $this->arParams['WRAPPER']['PAGEN'];
        $this->arResult['NAV_RESULT']->NavPageCount = 3;
        $this->arResult['isNext'] = $this->arResult['NAV_RESULT']->NavPageNomer < $this->arResult['NAV_RESULT']->NavPageCount;
        $this->includeComponentTemplate();
    }

}
