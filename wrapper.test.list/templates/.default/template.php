<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$showLazyLoad = $arResult['NAV_RESULT']->NavPageNomer != $arResult['NAV_RESULT']->NavPageCount;
?>
<div class="row">
    <div class="col">
        <div class="row <?= $arParams['WRAPPER']["CONTANER_LIST"] ?: '' ?>">
    <? foreach ($arResult["ITEMS"] as $arItem) { ?>
        <pre><? print_r($arItem) ?></pre>
    <? } ?>
        </div>
        <? if ($showLazyLoad) { ?>
            <div class="row btn-holder">
                <a class="btn-primary more1 <?= $arParams['WRAPPER']["CONTANER_MORE"] ?>">Показать еще больше</a>
            </div>
        <? } ?>
    </div>
</div>
<script>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(<?= $showLazyLoad ? 'true' : 'false' ?>);
</script>