<?php

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Page\Asset;

class ProjectAjaxWrapper extends CBitrixComponent
{

    private $isRun = false;

    private function startWrapperTemplate()
    {
        if (empty($this->arResult['IS_FULL'])) {
            return;
        }
        $jsParams = [
            'AJAX' => $this->arResult['AJAX'],
            'SITE_ID' => SITE_ID,
            'TEMPLATE_NAME' => $this->arResult['TEMPLATE_NAME'],
            'PARAM' => $this->arResult['PARAM'],
            'PAGEN' => $this->arResult['PAGEN'],
            'PAGE_URL' => $this->arResult['PAGE_URL'],
            'CONTANER' => '.' . $this->arResult['CONTANER'],
            'CONTANER_FORM' => '.' . $this->arResult['CONTANER_FORM'],
            'CONTANER_LIST' => '.' . $this->arResult['CONTANER_LIST'],
            'CONTANER_MORE' => '.' . $this->arResult['CONTANER_MORE'],
            'CONTANER_PAGER' => '.' . $this->arResult['CONTANER_PAGER'],
            'CONTANER_FILTER' => '.' . $this->arResult['CONTANER_FILTER'],
            'CONTANER_FILTER_ALL' => '.' . $this->arResult['CONTANER_FILTER_ALL'],
        ];
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> =
                new jsProjectAjaxWrapper(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent()
    {
        global $APPLICATION;
        if (!empty($this->arResult['IS_AJAX'])) {
            $this->initAjax();
        }
        $this->arResult['FORM_KEY'] = sha1($this->arParams['WEB_FORM_ID'] ?: $this->GetTemplateName() . $this->getSiteTemplateId());
        $this->arResult['FORM_KEY_HTML'] = '<input type="hidden" name="' . $this->arResult['FORM_KEY'] . '" value="1" />';

        $this->arResult['IS_RELOAD_PAGE'] = isset($this->arParams['IS_RELOAD_PAGE']) and $this->arParams['IS_RELOAD_PAGE'] == 'Y';
        if ($this->arResult['IS_RELOAD_PAGE']) {
            $request = Application::getInstance()->getContext()->getRequest();
            if ($request->get('action') === 'AJAX') {
                $this->arParams['IS_AJAX'] = 'Y';
                $this->arParams['IS_UPDATE'] = $request->get('IS_UPDATE');
                $this->arParams['PAGEN'] = $request->get('PAGEN_1') ?? 1;
                $this->arParams['WEB_FORM_ID'] = $request->get('WEB_FORM_ID');
                $this->arParams['FILTER'] = $request->get('FILTER');
                $this->arParams['PARAM'] = $request->get('PARAM');
                $this->arParams['PAGE_URL'] = $request->get('PAGE_URL');
                $APPLICATION->SetCurPage($this->arParams['PAGE_URL']);
                $_SERVER["REQUEST_URI"]  = $this->arParams['PAGE_URL'];
                unset($_GET['action'], $_GET['IS_UPDATE'], $_GET['TEMPLATE_NAME'], $_GET['AJAX'], $_GET['WEB_FORM_ID'], $_GET['FILTER'], $_GET['PARAM'], $_GET['PAGE_URL']);
            }
        }

        $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
        $this->arResult['IS_FULL'] = $this->arParams['IS_UPDATE'] ?: empty($this->arResult['IS_AJAX']);
        $this->arResult['PAGEN'] = ceil($this->arParams['PAGEN'] ?: 1);
        $this->arResult['PAGE_URL'] = $this->arParams['PAGE_URL'] ?: $APPLICATION->GetCurPage(false);
        $this->arResult['AJAX'] = $this->arResult['IS_RELOAD_PAGE'] ? $APPLICATION->GetCurPage(false) : $this->GetPath() . '/ajax.php';
        $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
        $this->arResult['FILTER'] = $this->arParams['FILTER'] ?: '';
        $this->arResult['PARAM'] = $this->arParams['PARAM'] ?: '';
        $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult['PARAM'] ?: '');

        $this->arResult['CONTANER'] = 'ajax-wrapper-contaner' . $key;
        $this->arResult['CONTANER_FORM'] = 'ajax-wrapper-form' . $key;
        $this->arResult['CONTANER_LIST'] = 'ajax-wrapper' . $key;
        $this->arResult['CONTANER_MORE'] = 'ajax-wrapper-more' . $key;
        $this->arResult['CONTANER_PAGER'] = 'ajax-wrapper-pager' . $key;
        $this->arResult['CONTANER_FILTER'] = 'ajax-wrapper-filter' . $key;
        $this->arResult['CONTANER_FILTER_ALL'] = 'ajax-wrapper-filter-all' . $key;
        $this->arResult['JS_OBJECT'] = 'jsWrapperList' . $key;

        $this->eventStart();
        $this->startWrapperTemplate();
        $this->includeComponentTemplate();
        $this->eventStop();
        if (empty($this->arResult['IS_AJAX'])) {
            Asset::getInstance()->addJs($this->GetPath() . '/loader.js');
            Asset::getInstance()->addJs($this->GetPath() . '/view.js');
            Asset::getInstance()->addJs($this->GetPath() . '/script.js');
        } else {
            $this->sendAjax();
        }
    }

    protected function eventStart()
    {
        if (empty($this->arResult['IS_AJAX'])) {
            return;
        }
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arResult['IS_FORM_SEND'] = ($request->isPost() && $request->getPost($this->arResult['FORM_KEY']));
        if ($this->arResult['IS_FORM_SEND']) {
            $this->isRun = true;
            EventManager::getInstance()->addEventHandlerCompatible('main', 'OnBeforeLocalRedirect',
                [&$this, 'eventRedirect']);
        }
    }

    public function eventRedirect($url)
    {
        static $count = 1;
        if (empty($this->isRun)) {
            return;
        }
        $param = parse_url($url);
        if (!empty($param['query'])) {
            parse_str($param['query'], $param);
            if (isset($this->arParams['WEB_FORM_ID']) && ($param['formresult'] ?? '') === 'addok' && ($param['WEB_FORM_ID'] ?? '') === $this->arParams['WEB_FORM_ID']) {
                unset($_POST['sessid']);
                Application::getInstance()->getContext()->getRequest()->set($_POST);
                $count++;
                if ($count > 2) {
                    exit;
                }
                $this->arResult['FORM_SEND'] = true;
            } else {
                $this->arResult['IS_REDIRECT'] = true;
            }
            $this->includeComponentTemplate();
            $this->sendAjax();
        }
    }

    protected function eventStop()
    {
        $this->isRun = false;
    }

    protected function sendAjax()
    {
        $arResult = [];
        $arResult['content'] = '<div>' . ob_get_clean() . '</div>';
        global $USER;
        $arResult['IsAuthorized'] = $USER->IsAuthorized();
        echo json_encode($arResult);
        exit;
    }

}