<?

use Bitrix\Main\Application;

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
if (isset($_POST['siteId'])) {
    define('SITE_ID', $_POST['siteId']);
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$request = Application::getInstance()->getContext()->getRequest();
$APPLICATION->IncludeComponent('project.ajax:wrapper', $request->get('TEMPLATE_NAME'), Array(
    'IS_AJAX'     => 'Y',
    'IS_UPDATE'   => $request->get('IS_UPDATE'),
    'PAGEN'       => $request->get('PAGEN_1') ?? 1,
    'WEB_FORM_ID' => $request->getPost('WEB_FORM_ID'),
    'FILTER'      => $request->get('FILTER'),
    'PARAM'       => $request->get('PARAM'),
));