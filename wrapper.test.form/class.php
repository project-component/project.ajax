<?php

use Bitrix\Main\Application;

class ProjectAjaxWrapperTestForm extends CBitrixComponent {

    public function executeComponent() {
        $arParams = $this->arParams;
        $arParams['FILTER'] = $arParams['WRAPPER']['FILTER'] ?? '';
        $arParams['PAGEN'] = $arParams['WRAPPER']['PAGEN'] ?? '';
        unset($arParams['WRAPPER'], $arParams['~WRAPPER']);

        $this->arResult['ERROR'] = array();
        $this->arResult['HASH'] = sha1(serialize($arParams));
        if (!empty($this->arParams['WRAPPER']['IS_AJAX'])) {
            $request = Application::getInstance()->getContext()->getRequest();
            if ($request->getPost('hash') === $this->arResult['HASH']) {
                if ($request->getPost('email')) {
                    $this->arResult['IS_SEND'] = true;
                } else {
                    $this->arResult['ERROR']['MESSAGE'] = 'Поле «Email» не заполнено';
                }
            }
        }
        $this->includeComponentTemplate();
    }

}
